# Local development


## Project structure
- `config/` -
- `images/` - dockerfiles for custom images
- `scripts/` - bash scripts for different targets
- `data/` - ignored folder, inlcudes data from docker images (database, redis)
- `hwm-helper/` - main app forlder, **ignored**
- `hwm-cookbooks/` - cookbooks, **ignored**
- `hwm-bot-api/` - nodejs bot, **ignored**
- `.env` - variables for configurate environment (Docker)

> `hwm-helper`, `hwm-cookbooks`, `hwm-bot-api` - must cloned manual

### Install
- `shell scripts/import_repositories` - import main repositories

- `./dc up -d` - run docker
> auto run command: `compoesr install` `npm i` `php artisan migrate --seed`

- `./dc exec -u docker app bash` - enter inside container
> Important parameter *-u docker* for correct permissions

`./dc exec app php artisan migrate --seed` - run migrations
