#!/bin/bash

HWM_HELPER_REPO=git@gitlab.com:lordxaart/hwm-helper.git
HWM_COOKBOOKS_REPO=git@gitlab.com:lordxaart/hwm-cookbooks.git
HWM_BOT_REPO=git@gitlab.com:lordxaart/hwm-bot-api.git

read -p "Enter HWM HELPER repository url [$HWM_HELPER_REPO]:" repo_app
repo_app=${name:-$HWM_HELPER_REPO}

read -p "Enter COOKBOOKS repository url [$HWM_COOKBOOKS_REPO]:" repo_cookbooks
repo_cookbooks=${name:-$HWM_COOKBOOKS_REPO}

read -p "Enter BOT repository url [$HWM_BOT_REPO]:" repo_bot
repo_bot=${name:-$HWM_BOT_REPO}

echo ""

echo "Cloning app repo [$repo_app] ..."
git clone $repo_app
echo "DONE"
echo ""

echo "Cloning cookbooks repo [$repo_cookbooks] ..."
git clone $repo_cookbooks
echo "DONE";
echo ""

echo "Cloning bot repo [$repo_bot] ..."
git clone $repo_bot
echo "DONE";
echo ""
