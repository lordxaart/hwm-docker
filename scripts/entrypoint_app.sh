#!/bin/bash

set -eu

# nginx template
echo "Rendering nginx templates"
/root/scripts/render-templates.sh /etc/nginx/sites-templates /etc/nginx/sites-enabled

# composer
if [ ! -d "vendor" ]
then
  composer install --no-interaction || true
fi

cd $APP_PATH

php artisan key:generate
php artisan storage:link

#echo fs.inotify.max_user_watches=524288 | tee -a /etc/sysctl.conf

service php$PHP_VERSION-fpm start
service supervisor start
service cron start
redis-server --daemonize yes

exit $@