FROM node:16.14.0

# Requirements for puppeteer.js
ADD ./config/puppeteer_requirements.txt /root
RUN apt-get update && apt-get install -yq $(cat /root/puppeteer_requirements.txt)

EXPOSE 3000

CMD ["npm run watch"]
