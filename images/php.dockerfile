ARG PHP_VERSION

FROM php:${PHP_VERSION}-fpm

RUN apt-get update -y

RUN apt-get install -yq git curl wget nano

RUN apt-get install -y libicu-dev --no-install-recommends

RUN docker-php-ext-configure intl && docker-php-ext-install intl
RUN docker-php-ext-install bcmath
RUN docker-php-ext-install pdo_mysql pdo

# Imagick
RUN apt-get install -y libmagickwand-dev --no-install-recommends
RUN pecl install imagick && docker-php-ext-enable imagick

#Install xdebug
RUN pecl install xdebug && docker-php-ext-enable xdebug

EXPOSE 9000

CMD ["php-fpm"]
