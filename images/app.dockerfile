FROM ubuntu:20.04

ARG PHP_VERSION
ARG APP_PATH

# Need for tzdata
ENV DEBIAN_FRONTEND noninteractive
ENV TZ=UTC

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Update all packages
RUN apt-get update

# Tools
RUN apt-get install -yq \
    git \
    apt-utils \
    curl \
    openssl \
    nano \
    iputils-ping

RUN apt-get install -y lsb-release gnupg2 ca-certificates apt-transport-https software-properties-common
RUN add-apt-repository ppa:ondrej/php
RUN apt-get install -yq php${PHP_VERSION}
RUN apt-get install -yq php${PHP_VERSION}-curl \
        php${PHP_VERSION}-mysql \
        php${PHP_VERSION}-imagick \
        php${PHP_VERSION}-bcmath \
        php${PHP_VERSION}-intl
RUN apt-get install -yq mysql-client imagemagick

RUN apt-get install -yq php${PHP_VERSION}-xml
RUN apt-get install -yq php${PHP_VERSION}-mbstring
RUN apt-get install -yq php${PHP_VERSION}-zip

# Install composer (only with php)
RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer

# Cron
RUN apt-get -y install cron

# Supervisor
RUN apt-get install -yq supervisor
RUN mkdir -p /var/log/supervisor

# tool for image optimize
RUN apt-get install -yq \
    jpegoptim \
    optipng \
    pngquant \
    gifsicle \
    webp
# RUN npm install -g svgo

# Install git lfs
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
RUN apt-get install -yq git-lfs

ADD ./config/crontab /etc/cron.d/crontab
RUN chmod 0644 /etc/cron.d/crontab
RUN crontab /etc/cron.d/crontab
RUN touch /var/log/cron.log
RUN crontab /etc/cron.d/crontab

# Install XDebug
#RUN apt-get update
#RUN apt-get install -yq php${PHP_VERSION}-xdebug

# Nginx
RUN apt-get install -yq nginx
RUN apt-get install -yq php${PHP_VERSION}-fpm

# Redis
RUN apt-get install -yq redis-server
EXPOSE 6379

# Node.js
#RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
#RUN apt-get install nodejs -y

RUN mkdir ${APP_PATH}
WORKDIR ${APP_PATH}

# https://github.com/gatsbyjs/gatsby/issues/11406#issuecomment-458769756
# RUN echo fs.inotify.max_user_watches=524288 | tee -a /etc/sysctl.conf

CMD sh /root/scripts/entrypoint_app.sh ; nginx -g 'daemon off;' ;
CMD sh /root/scripts/entrypoint_app.sh ; nginx -g 'daemon off;' ;
